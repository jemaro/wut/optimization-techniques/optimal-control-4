# AMPL MODEL file for Optimal Control 4

# option solver conopt
# option solver loqo
option solver minos
# option solver snopt

# Initial parameters
param t0;
param tf;
param N;
param x_ini {n in 1..2};

# Calculated parameters
param Delta = (tf - t0) / N;
param t {k in 0..(N-1)} = t0+k*Delta;
# This parameter is not being used in the solution, it is computed only to plot
# the constrain values. It is auxiliary and can be commented out
param x2_max {k in 0..(N-1)} = 8*(t[k] - 0.5)^2 - 0.5;

## Decision variable vectors
# Control
var u {k in 0..(N-1)};
# Dynamic object
var x1 {k in 0..(N-1)};
var x2 {k in 0..(N-1)};

# Performance index
minimize J: Delta * sum {k in 0..(N-1)} (x1[k]^2 + x2[k]^2 + 0.005*(u[k]^2));

# State constraint
subject to state {k in 0..(N-1)}:
     x2[k] <= 8*(t[k] - 0.5)^2 - 0.5
;

# ODE constrains
subject to x2_ODE {k in 1..(N-1)}:
     x2[k] = ((1 - Delta)*x2[k-1] + Delta*u[k-1])
;
subject to x1_ODE {k in 1..(N-1)}:
     x1[k] = (x1[k-1] + Delta*x2[k-1])
;

# Initial conditions
subject to x2_initial:
     x2[0] = x_ini[2]
;
subject to x1_initial:
     x1[0] = x_ini[1]
;